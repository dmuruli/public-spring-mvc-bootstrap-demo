<%@tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ attribute name="header" fragment="true" %>
<html>
<jsp:invoke fragment="header">
    <head>

        <title>David Muruli Hermes Demo</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">

        <meta name="description" content="Demo by David Muruli to illustrate use of Spring MVC, Hibernate and Bootstrap ">

        <meta name="keywords" content="David Muruli,bootstrap, admin bootstrap, S" />

        <!-- Core CSS - Include with every page -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Dashboard -->
        <link href="css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
        <link href="css/plugins/timeline/timeline.css" rel="stylesheet">

        <!-- Mint Admin CSS - Include with every page -->
        <link href="css/mint-admin.css" rel="stylesheet">

        <!-- Custom Work Order Layout-->
        <link href="css/workorder.css" rel="stylesheet">

    </head>
</jsp:invoke>
    <body class="wobackground">
  <jsp:doBody/>
</body></html>