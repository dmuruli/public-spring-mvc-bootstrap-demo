<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

    <head>

        <title>David Muruli Hermes Demo</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">

        <meta name="description" content="Demo by David Muruli to illustrate use of Spring MVC, Hibernate and Bootstrap ">

        <meta name="keywords" content="David Muruli,bootstrap, admin bootstrap, S" />

        <link rel="author" href="http://grozav.com"/>

        <!-- Core CSS - Include with every page -->
        <link href="resources/css/bootstrap.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Dashboard -->
        <link href="resources/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
        <link href="resources/css/plugins/timeline/timeline.css" rel="stylesheet">

        <!-- Mint Admin CSS - Include with every page -->
        <link href="resources/css/mint-admin.css" rel="stylesheet">

        <!-- Custom Work Order Layout-->
        <link href="resources/css/workorder.css" rel="stylesheet">

    </head>

    <body class="wobackground">

        <div id="wrapper">

            <nav class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img class="brand-logo" src="resources/img/logo.png" alt=""/>Hermes Demo</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <li>John Doe</li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

            </nav>
            <!-- /.navbar-static-top -->

            <div id="page" class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header text-asbestos">Dashboard</h3>
                    </div>
                </div>
                <!-- /.col-lg-12 -->

                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-primary text-center panel-eyecandy">
                            <div class="panel-body asbestos">
                                <i class="fa fa-bar-chart-o fa-3x"></i>
                                <h3><c:out value="${dashboardView.assignedWorkOrdersCount}"/> <i class="fa fa-arrow-circle-o-down"></i></h3>
                            </div>
                            <div class="panel-footer">
                                <span class="panel-eyecandy-title">
                                   <a href="assignedworkorders.html">Assigned Work Orders</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-xs-6 col-md-3 -->
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-primary text-center panel-eyecandy">
                            <div class="panel-body theme-color">
                                <i class="fa fa-shopping-cart fa-3x"></i>
                                <h3><c:out value ="${dashboardView.openWorkOrdersCount}"/> <i class="fa fa-arrow-circle-o-up"></i></h3>
                            </div>
                            <div class="panel-footer">
                                <span class="panel-eyecandy-title">
                                    <a href="openworkorders.html">Open Work Orders</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-xs-6 col-md-3 -->
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-primary text-center panel-eyecandy">
                            <div class="panel-body asbestos">
                                <i class="fa fa-users fa-3x"></i>
                                <h3><c:out value ="${dashboardView.completeWorkOrdersCount}"/> <i class="fa fa-arrow-circle-o-down"></i></h3>
                            </div>
                            <div class="panel-footer">
                                <span class="panel-eyecandy-title">
                                    <a href="completedWorkOrders.html">Completed Work Orders</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-xs-6 col-md-3 -->
                    <div class="col-xs-6 col-md-3">
                        <div class="panel panel-primary text-center panel-eyecandy">
                            <div class="panel-body theme-color">
                                <i class="fa fa-comments fa-3x"></i>
                                <h3><c:out value ="${dashboardView.closedWorkOrdersCount}"/><i class="fa fa-arrow-circle-o-up"></i></h3>
                            </div>
                            <div class="panel-footer">
                                <span class="panel-eyecandy-title">
                                    <a href="completedworkorders.html" ></a>Closed Work Orders
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-xs-6 col-md-3 -->
                </div>


                <!-- /.row -->
                <!-- /.row -->
            </div>
            <!-- /#container -->

        </div>
        <!-- /#wrapper -->

        <!-- Core Scripts - Include with every page -->
        <script src="resources/js/jquery-1.10.2.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Page-Level Plugin Scripts - Dashboard -->
        <script src="resources/js/plugins/morris/raphael-2.1.0.min.js"></script>
        <script src="resources/js/plugins/morris/morris.js"></script>

        <!-- Mint Admin Scripts - Include with every page -->
        <script src="resources/js/mint-admin.js"></script>

        <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
        <script src="resources/js/demo/dashboard-demo.js"></script>

    </body>

</html>
