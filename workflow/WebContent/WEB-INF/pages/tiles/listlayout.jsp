<!DOCTYPE html>
<%@ taglib tagdir="/WEB-INF/tags" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>

    <head>

        <title><tiles:getAsString name="title"/></title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<tiles:insertAttribute name="resources" />

    </head>

    <body class="wobackground" >
		
        <div id="wrapper">
			<tiles:insertAttribute name="header" />
            <div id="page" class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header"><tiles:getAsString name="user"/></h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <tiles:getAsString name="pageTitle"/>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="listTable">
                                        <thead>
                                            <tr>
                                                <th>Work Order No.</th>
                                                <th>Work Order Contact</th>
                                                <th>Work Requested</th>
                                                <th>Create Date</th>
                                                <th>Assign Date</th>
                                                <th>Assign To</th>
                                                <th>Expected Completion Date</th>
                                                <th>Current Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>Trident</td>
                                                <td>Internet Explorer 4.0</td>
                                                <td>Win 95+</td>
                                                <td class="center">4</td>
                                                <td class="center">X</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="even gradeC">
                                                <td>Trident</td>
                                                <td>Internet Explorer 5.0</td>
                                                <td>Win 95+</td>
                                                <td class="center">5</td>
                                                <td class="center">C</td>
                                                <td>&nbsp;</td>
												<td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr class="gradeU">
                                                <td>Other browsers</td>
                                                <td>All others</td>
                                                <td>-</td>
                                                <td class="center">-</td>
                                                <td class="center">U</td>
                                                <td>&nbsp;</td>
												<td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>

                                            <c:forEach items="${completedWorkOrders}" var="workOrder">
                                            	<tr class="gradeX">
                                            	<td><c:out value="${workOrder.workOrderId}" /></td>
                                            	<td><c:out value="${workOrder.contactName}" /></td>
                                            	<td>&nbsp;</td>
                                            	<td> <fmt:formatDate type="date" value="${workOrder.createdTimeStamp}" /></td>
                                            	<td>&nbsp;</td>
                                            	<td>&nbsp;</td>
                                            	<td>&nbsp;</td>
                                            	<td><c:out value="${workOrder.workOrderStatus.statusName}" /></td>
                                            	</tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#assignedWorkOrdersTable').dataTable();
                var table = $('#listTable').DataTable();
                $('#listTable tbody').on( 'dblclick', 'tr', function () {
                	
                   // alert( 'Row index: '+table.row( this ).index() );
		             var rowdata  =table.row( this ).data();
                   // alert('Data in cell 1 is workorder ID: ' +rowdata[0]);
                    window.location.href="workorderview.html";
                    <!-- window.location.href = file2.html for same page -->
                } );
            });
        </script>

    </body>

</html>
