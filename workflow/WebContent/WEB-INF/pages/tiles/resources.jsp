<head>

        <title>WorkFlow Job Scheduling Demo</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="author" href="https://twitter.com/dsmuruli"/>

        <!-- Core CSS - Include with every page -->
        <link href="resources/css/bootstrap.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Tables -->
        <link href="resources/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- Mint Admin CSS - Include with every page -->
        <link href="resources/css/mint-admin.css" rel="stylesheet">
          <!-- Custom Work Order Layout-->
        <link href="resources/css/workorder.css" rel="stylesheet">
        
        
        <!-- Core Scripts - Include with every page -->
        <script src="resources/js/jquery-1.10.2.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Page-Level Plugin Scripts - Tables -->
        <script src="resources/js/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="resources/js/plugins/dataTables/dataTables.bootstrap.js"></script>

        <!-- Mint Admin Scripts - Include with every page -->
        <script src="resources/js/mint-admin.js"></script>
        
        

    </head>
