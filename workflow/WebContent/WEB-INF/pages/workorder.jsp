<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

    <head>

        <title>David Muruli</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">

        <meta name="description" content="Example Work Order Demo.  Illustrated bootstrap, Spring MVC and Hibernate">

        <!-- Core CSS - Include with every page -->
        <link href="resources/css/bootstrap.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="resources/css/datepicker.css" rel="stylesheet">

        <!-- Page-Level Plugin CSS - Forms -->
        <!-- Mint Admin CSS - Include with every page -->
        <link href="resources/css/mint-admin.css" rel="stylesheet">
                  <!-- Custom Work Order Layout-->
        <link href="resources/css/workorder.css" rel="stylesheet">
     
        <!-- Core Scripts - Include with every page -->
        <script src="resources/js/jquery-1.10.2.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>
   <script src="resources/js/bootstrap-datepicker.js"></script>
        
        <!-- Page-Level Plugin Scripts - Forms -->

        <!-- Mint Admin Scripts - Include with every page -->
        <script src="resources/js/mint-admin.js"></script>
        
         <script>
       
    $("#dtReported").datepicker({	
    format: "mm/dd/yyyy",
    startDate: "01-01-2014",
    endDate: "01-01-2015",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
    });
    </script>

        <!-- Page-Level Demo Scripts - Forms - Use for reference -->
    </head>

    <body class="wobackground">
        <div id="wrapper">
			<div id="page" class="container">
         
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Work Order</h3>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Work Order
                            </div>
                            <div class="panel-body">
                            <form:form role="form" commandName="workOrderView" action="submitWorkOrder.html" method="post">
                            
                                <div class="row">
                                    <div class="col-lg-4">
                                        
                                            <div class="form-group">
                                                <label>Customer </label>
                                                <form:select path="workOrder.customerId" class="form-control">
                                                 <option value="">---SELECT---------------</option>
                                                <form:options items="${customerList}" itemLabel="companyName" itemValue="customerId"/>
                                                </form:select>
                                             </div>
                                             <div class="form-group">
                                                <label>Customer Contact</label>
                                                <input class="form-control"/>
                                 
                                            </div>
                                            
                                           <div class="form-group">
                                                <label>Customer Contact Phone:</label>
                                                <input class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Customer Contact e-mail:</label>
                                                <input class="form-control"/>
                                            </div>
                                               
                                            </div>
                                            <!-- /div col-lg-6 left --> 
                                     <div class="col-lg-4">
                                     	 <div class="form-group">	
                                     	 	<label>Date Reported </label>
                                                <form:input path="workOrder.dateReported" type="date" id="dtReported" placeholder="dd/mm/yyyy" class="form-control"/>
                                     	 </div>
                                     	 <div class="form-group">
                                     	 	<label>Date Assigned </label>
                                                <form:input path="workOrder.dateAssigned" type="date" class="form-control" placeholder="dd/mm/yyyy"/>
                                     	 </div>
                                     	 <div class="form-group">
                                     	 	<label>Assigned To: </label>
                                                <form:select path="selectedTechnician" class="form-control">
                                                 <option value="">---SELECT---------------</option>
                                                 <form:options itemLabel="fullName" itemValue="appUserId" items="${techs}"/>
                                                </form:select>
                                     	 </div>
                                     	
                                     </div>
                                     <!-- /div col-lg-6 right -->      
                                    </div>
                                    <div class ="row">
                                    <div class="col-lg-12">
                                     <div class="form-group">
                                                <label>Work Description</label>
                                                <form:textarea path="workOrder.description" class="form-control" rows="3"></form:textarea>
                                    </div>
                                     <div class="form-group">
                                                <label>Notes</label>
                                                <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                     	 	<label>Work Order Status</label>
                                                <form:select path="workOrder.workOrderStatus.workOrderStatusId" class="form-control">
                                                 <option value="">---SELECT---------------</option>
                                                <form:options items="${workOrderStatusList}" itemLabel="statusName" itemValue="workOrderStatusId"/>
                                                </form:select>
                                     	 </div>
                                    <div class="form-group">
                                     	 	<label>Date Completed </label>
                                                <input class="form-control"/>
                                     	 </div>
                                    </div>
                                	</div>
                                	<!-- End of row -->
    							<div id="buttons" class="row" >
    							 <button type="submit" class="btn btn-default">Submit Button</button>
                                 <button type="reset" class="btn btn-default">Reset Button</button>
                                     
    							</div>
                                </div>
                                
                                <!-- /.row (nested) -->
                                   
                                   
                            </div>
                            
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                    </form:form>
                </div>
                <!-- /.row -->

			</div>
        </div>
        <!-- /#wrapper -->


    </body>

</html>
