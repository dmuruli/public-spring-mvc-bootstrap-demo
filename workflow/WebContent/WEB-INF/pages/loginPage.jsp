<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>


    <head>

        <title>Hermes Demo Login</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">


        <!-- Core CSS - Include with every page -->
        <link href="resources/css/bootstrap.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

        <!-- Mint Admin CSS - Include with every page -->
        <link href="resources/css/mint-admin.css" rel="stylesheet">
		<link href="resources/css/workorder.css" rel="stylesheet">
    </head>

    <body class="wobackground">

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                 <!--  Page  Message Area -->
                   
                    <div class="login-panel panel panel-default">
                    <c:if test="${!empty pageError}">
                     <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
                                    <c:out value="${pageError }"></c:out>
                    </div>
                   </c:if>
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        
                  
                      
                        <div class="panel-body">
                            <form:form role="form" commandName="user" action="login.html" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <form:input class="form-control" placeholder="E-mail" path="email" type="email" autofocus="true"/>
                                    </div>
                                    <div class="form-group">
                                        <form:input class="form-control" placeholder="Password" path="password" type="password" value=""/>
                                    </div>
                                    
                                    <!-- Change this to a button or input when using this as a form -->
                                    <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
                                </fieldset>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Core Scripts - Include with every page -->
        <script src="resources/js/jquery-1.10.2.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Mint Admin Scripts - Include with every page -->
        <script src="resources/js/mint-admin.js"></script>

    </body>

</html>
