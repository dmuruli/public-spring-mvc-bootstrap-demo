<!DOCTYPE html>
<html>

    <head>

        <title>Work Order Workflow Demo</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="author" href="http://grozav.com"/>

        <!-- Core CSS - Include with every page -->
        <link href="resources/css/bootstrap.css" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

        <!-- Mint Admin CSS - Include with every page -->
        <link href="resources/css/mint-admin.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class='panel-heading text-center'>
                            <h1 class='large-error'>500</h1>
                            <h4>Oops, something went wrong!</h4>
                        </div>
                        <div class="panel-body text-center">
                            <a class='btn btn-block btn-primary' href='home.html'>Go to Backend</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Core Scripts - Include with every page -->
        <script src="resources/js/jquery-1.10.2.js"></script>
        <script src="resources/js/bootstrap.min.js"></script>
        <script src="resources/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Mint Admin Scripts - Include with every page -->
        <script src="resources/js/mint-admin.js"></script>

    </body>

</html>
