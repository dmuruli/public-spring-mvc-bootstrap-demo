package com.dmuruli.hermes.dao.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dmuruli.hermes.dao.WorkOrderDao;
import com.dmuruli.hermes.entity.WorkOrder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:testContext.xml")
public class WorkOrderDaoTest {
	@Autowired
	WorkOrderDao workOrderDao;
	long TEST_USER_JIM_TECHNICIAN=4;
	long CREATE_STATUS=1;
	long ASSIGN_STATUS=2;
	long OPEN_STATUS=3;
	long COMPLETE_STATUS=4;
	long CLOSED_STATUS=5;
	long DEFAULT_EXPECTED_WORK_ORDER_STATUS_COUNT=4;
	@Test
	public void testFindAllWorkOrders(){

		List<WorkOrder> workOrders =workOrderDao.findAll();
		System.out.println(" No. of work orders: " + workOrders.size());
		assertTrue( "No work orders retrieved",workOrders.size()>0);
	}
	@Test
	public void testFindAssignedWorkOrders(){
		long ASSIGNED_STATUS=2;
		long TEST_USER_JIM_TECHNICIAN=4;
		int EXPECTED_COUNT=4;
		List<WorkOrder>assignedWorkOrders = workOrderDao.findWorkOrdersByStatus(TEST_USER_JIM_TECHNICIAN, ASSIGNED_STATUS);
		assertTrue( "No assigned work orders retrieved",assignedWorkOrders.size()>0);
		assertTrue( "No of expected assigned work orders is 4 but found " + assignedWorkOrders.size(),
				assignedWorkOrders.size()==EXPECTED_COUNT);
	}
	@Test
	public void testFindCompletedWorkOrders(){
		long COMPLETED_STATUS=4;
		long TEST_USER_JIM_TECHNICIAN=4;
		List<WorkOrder>assignedWorkOrders = workOrderDao.findWorkOrdersByStatus(TEST_USER_JIM_TECHNICIAN, COMPLETED_STATUS);
		assertTrue( "No completed work orders retrieved",assignedWorkOrders.size()>0);
		assertTrue( "No of expected assigned work orders is 3 but found " + assignedWorkOrders.size(),
				assignedWorkOrders.size()==4);
	}
	@Test
	public void testFindOpenWorkOrders(){
		long OPEN_STATUS=3;
		long TEST_USER_JIM_TECHNICIAN=4;
		List<WorkOrder>assignedWorkOrders = workOrderDao.findWorkOrdersByStatus(TEST_USER_JIM_TECHNICIAN, OPEN_STATUS);
		assertTrue( "No open work orders retrieved",assignedWorkOrders.size()>0);
		assertTrue( "No of expected work orders with status open is 3 but found " + assignedWorkOrders.size(),
				assignedWorkOrders.size()==4);
	}
	@Test
	public void testFindClosedWorkOrders(){
		long CLOSED_STATUS=5;
		long TEST_USER_JIM_TECHNICIAN=4;
		List<WorkOrder>assignedWorkOrders = workOrderDao.findWorkOrdersByStatus(TEST_USER_JIM_TECHNICIAN, CLOSED_STATUS);
		assertTrue( "No closed work orders retrieved",assignedWorkOrders.size()>0);
		assertTrue( "No of expected closed work orders is 4 but found " + assignedWorkOrders.size(),
				assignedWorkOrders.size()==4);
	}

	@Test
	public void testFindAssignedWorkOrdersCount(){
		int assingedWorkOrdersCount =workOrderDao.findAssignedWorkOrdersCount(TEST_USER_JIM_TECHNICIAN);
		assertTrue("Assigned Work Orders Count should be 4 but it is "+assingedWorkOrdersCount,assingedWorkOrdersCount==DEFAULT_EXPECTED_WORK_ORDER_STATUS_COUNT );
	}
	@Test
	public void testFindCompletedWorkOrdersCount(){
		int EXPECTED_COUNT =8;
		int completedWorkOrdersCount = workOrderDao.findWorkOrdersByStatusCount(TEST_USER_JIM_TECHNICIAN, COMPLETE_STATUS);
		assertTrue("Expected "+ DEFAULT_EXPECTED_WORK_ORDER_STATUS_COUNT +" Work Orders with completed status but found "
		+ completedWorkOrdersCount, 
		EXPECTED_COUNT==completedWorkOrdersCount);
	}
	@Test
	public void testFindCreateWorkOrdersCount(){
		int CREATE_WORK_ORDER_STATUS_COUNT =3;
		int createWorkOrdersCount = workOrderDao.findWorkOrdersByStatusCount(TEST_USER_JIM_TECHNICIAN, CREATE_STATUS);
		assertTrue("Expected "+ CREATE_WORK_ORDER_STATUS_COUNT +" Work Orders with created status but found "+ createWorkOrdersCount,
				CREATE_WORK_ORDER_STATUS_COUNT==createWorkOrdersCount);
	}
	@Test
	public void testFindOpenWorkOrdersCount(){
		int openWorkOrdersCount = workOrderDao.findWorkOrdersByStatusCount(TEST_USER_JIM_TECHNICIAN, OPEN_STATUS);
		assertTrue("Expected "+ DEFAULT_EXPECTED_WORK_ORDER_STATUS_COUNT +" Work Orders with open status but found "+ openWorkOrdersCount,
				DEFAULT_EXPECTED_WORK_ORDER_STATUS_COUNT==openWorkOrdersCount);
	}
	@Test
	public void testFindClosedWorkOrdersCount(){
		int closedWorkOrdersCount = workOrderDao.findWorkOrdersByStatusCount(TEST_USER_JIM_TECHNICIAN,CLOSED_STATUS);
		assertTrue("Expected "+ DEFAULT_EXPECTED_WORK_ORDER_STATUS_COUNT +" Work Orders with closed status but found "+ closedWorkOrdersCount,
				DEFAULT_EXPECTED_WORK_ORDER_STATUS_COUNT==closedWorkOrdersCount);
	}


}
