package com.dmuruli.hermes.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
@Entity
@Table(name = "CUSTOMER")
@NamedQueries({
	@NamedQuery(name="findAllCustomers", query="select c From Customer c")
})
public class Customer extends BaseEntity implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CUSTOMER_ID", nullable=false, unique=true)
	Long customerId;
	@Column(name="COMPANY_NAME")
	String companyName;
	@Column(name="CONTACT_FIRST_NAME")
	String customerContactFirstName;
	@Column(name="CONTACT_LAST_NAME")
	String customerContactLastName;
	@Column(name="CONTACT_PHONE")
	String customerContactPhone;
	@Column(name="CUSTOMER_PHONE")
	String customerPhone;
	@ManyToMany(fetch=FetchType.LAZY)
	  @JoinTable(name="CUSTOMER_ADDRESS",
	  joinColumns={@JoinColumn(name="CUSTOMER_ID", referencedColumnName="CUSTOMER_ID")},
	  inverseJoinColumns={@JoinColumn(name="ADDRESS_ID", referencedColumnName="ADDRESS_ID")})
	List<Address> locations;
	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	/**
	 * @return the customerContactFirstName
	 */
	public String getCustomerContactFirstName() {
		return customerContactFirstName;
	}
	/**
	 * @param customerContactFirstName the customerContactFirstName to set
	 */
	public void setCustomerContactFirstName(String customerContactFirstName) {
		this.customerContactFirstName = customerContactFirstName;
	}
	/**
	 * @return the customerContactLastName
	 */
	public String getCustomerContactLastName() {
		return customerContactLastName;
	}
	/**
	 * @param customerContactLastName the customerContactLastName to set
	 */
	public void setCustomerContactLastName(String customerContactLastName) {
		this.customerContactLastName = customerContactLastName;
	}
	/**
	 * @return the customerContactPhone
	 */
	public String getCustomerContactPhone() {
		return customerContactPhone;
	}
	/**
	 * @param customerContactPhone the customerContactPhone to set
	 */
	public void setCustomerContactPhone(String customerContactPhone) {
		this.customerContactPhone = customerContactPhone;
	}
	/**
	 * @return the customerPhone
	 */
	public String getCustomerPhone() {
		return customerPhone;
	}
	/**
	 * @param customerPhone the customerPhone to set
	 */
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	/**
	 * @return the locations
	 */
	public List<Address> getLocations() {
		return locations;
	}
	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<Address> locations) {
		this.locations = locations;
	}
	

}
