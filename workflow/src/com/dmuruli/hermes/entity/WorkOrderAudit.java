package com.dmuruli.hermes.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "WORK_ORDER_AUDIT")
public class WorkOrderAudit extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5941743364644715243L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="WORK_ORDER_AUDIT_ID", nullable=false, unique=true)
	Long workOrderAuditId;
	@Column(name="WORK_ORDER_ID")
	Long workOrderId;
	@Column(name="MESSAGE")
	String message;
	@Column(name="STATUS")
	WorkOrderStatus status;
	

}
