package com.dmuruli.hermes.entity;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: AppUser
 *
 */
@Entity
@Table(name = "APP_USER")
@NamedQueries({
	@NamedQuery(name="findAllExpeditors",query="select e from AppUser e where e.role.roleId=1"),
	 @NamedQuery(name="findAllTechs",query="select e from AppUser e where e.role.roleId=2")
	
})
public class AppUser  extends BaseEntity implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="APP_USER_ID")
	private Long appUserId;
	@Column(name="EMAIL")
	private String email;
	@Column(name="FIRST_NAME")
	private String firstName;
	@Column(name="LAST_NAME")
	private String lastName;
	@ManyToMany(fetch=FetchType.LAZY ,mappedBy="technicians")
	private List<WorkOrder> workorders;
	@Column(name="CONTACT_ADDRESS")
	private Address contactAddress;
	@Column(name="CONTACT_PHONE_NO")
	private String contactPhone;

	@Column(name="PASSWORD")
	private String password;
	@Column(name="VERIFIED")
	private Boolean verified;
	@Column(name="LOCKED")
	private boolean locked;
	@OneToOne(fetch=FetchType.EAGER,optional = true)
	@JoinColumn(name="ROLE_ID")
	private Role role;
	private static final long serialVersionUID = 1L;

	public AppUser() {
		super();
	}   
	public Long getAppUserId() {
		return this.appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}   
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
	
	
	public Address getContactAddress() {
		return this.contactAddress;
	}

	public void setContactAddress(Address contactAddress) {
		this.contactAddress = contactAddress;
	}   
	public String getContactPhone() {
		return this.contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the workorders
	 */
	public List<WorkOrder> getWorkorders() {
		return workorders;
	}
	/**
	 * @param workorders the workorders to set
	 */
	public void setWorkorders(List<WorkOrder> workorders) {
		this.workorders = workorders;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((appUserId == null) ? 0 : appUserId.hashCode());
		result = prime * result
				+ ((contactAddress == null) ? 0 : contactAddress.hashCode());
		result = prime * result
				+ ((contactPhone == null) ? 0 : contactPhone.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + (locked ? 1231 : 1237);
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result
				+ ((verified == null) ? 0 : verified.hashCode());
		result = prime * result
				+ ((workorders == null) ? 0 : workorders.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof AppUser))
			return false;
		AppUser other = (AppUser) obj;
		if (appUserId == null) {
			if (other.appUserId != null)
				return false;
		} else if (!appUserId.equals(other.appUserId))
			return false;
		if (contactAddress == null) {
			if (other.contactAddress != null)
				return false;
		} else if (!contactAddress.equals(other.contactAddress))
			return false;
		if (contactPhone == null) {
			if (other.contactPhone != null)
				return false;
		} else if (!contactPhone.equals(other.contactPhone))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (locked != other.locked)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (verified == null) {
			if (other.verified != null)
				return false;
		} else if (!verified.equals(other.verified))
			return false;
		if (workorders == null) {
			if (other.workorders != null)
				return false;
		} else if (!workorders.equals(other.workorders))
			return false;
		return true;
	}

	public String getFullName(){
		return this.firstName + " " + this.lastName;
	}
   
}
