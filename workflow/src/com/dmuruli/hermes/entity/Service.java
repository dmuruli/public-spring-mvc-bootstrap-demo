package com.dmuruli.hermes.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
@Entity
@DiscriminatorValue("S")
public class Service extends Product implements Serializable {
}
