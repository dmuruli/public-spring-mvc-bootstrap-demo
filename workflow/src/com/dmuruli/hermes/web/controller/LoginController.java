package com.dmuruli.hermes.web.controller;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dmuruli.hermes.entity.AppUser;
import com.dmuruli.hermes.entity.WorkOrder;
import com.dmuruli.hermes.web.validation.UserValidator;


/**
 * 
 * @author dmuruli
 * Example class illustrating use of 
 */
@Controller
public class LoginController
{

private static final Logger logger = Logger.getLogger(LoginController.class);
@Autowired
private UserValidator userValidator;

@InitBinder("user")
private void initBinder(WebDataBinder binder) {
	binder.setValidator(userValidator);
}

@RequestMapping(value="/")
public ModelAndView start(){
	return initLogin();
}

@RequestMapping(value="/home")
public ModelAndView initLogin(){
	logger.info("Init Login");
	ModelAndView mav = new ModelAndView("loginPage");
	AppUser appUser  =new AppUser();
	mav.addObject("user", appUser);
	return mav;
}


@RequestMapping(value="/login")
public ModelAndView login(@ModelAttribute @Valid AppUser user,
		BindingResult result){
  logger.info("The user email is: " + user.getEmail() +" the password is : " + user.getPassword());
  ModelAndView mav = new ModelAndView();
  if (result.hasErrors()){
	  mav.setViewName("loginPage");
	  logger.info("Validation failed, email not valid");
	  user.setPassword("");
	  mav.addObject("user", user);
	  String pageError ="The login does not exist in the system, "
	  		+ user .getEmail() +" contact "
	  		+ "Administrator(dsmuruli@yahoo.com) if you "
	  		+ "do not have a valid login";
	  mav.addObject("pageError",pageError);
	  return mav; 
  }else if(user!=null && user.getEmail().trim().equalsIgnoreCase("admin1@tech.com")){
	  
	  mav.setViewName("redirect:/createWorkOrder");
	  WorkOrder workOrder = new WorkOrder();
	  mav.addObject("workOrder", workOrder);
  }else{
	  String successview ="redirect:/techdashboard";
	  mav.setViewName(successview);
  }
  

  
  return mav;
} 
}