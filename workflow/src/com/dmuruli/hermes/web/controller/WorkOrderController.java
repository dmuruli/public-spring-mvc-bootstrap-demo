package com.dmuruli.hermes.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dmuruli.hermes.dao.WorkOrderDao;
import com.dmuruli.hermes.entity.AppUser;
import com.dmuruli.hermes.entity.Customer;
import com.dmuruli.hermes.entity.WorkOrder;
import com.dmuruli.hermes.entity.WorkOrderStatus;
import com.dmuruli.hermes.service.AppUserService;
import com.dmuruli.hermes.service.CustomerService;
import com.dmuruli.hermes.service.WorkOrderService;
import com.dmuruli.hermes.web.view.WorkOrderDashboardView;
import com.dmuruli.hermes.web.view.WorkOrderView;

@Controller
public class WorkOrderController {
	private static final Logger logger = Logger.getLogger(WorkOrderController.class);
	
	@Autowired
	private WorkOrderService workOrderService;
	@Autowired
	CustomerService customerService;
	@Autowired
	private AppUserService appUserService;
	
	/**
	 * Set a custom editor to handle the date coming from the web page
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    //sdf.setLenient(true);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}
	@RequestMapping(value="/techdashboard")
	public ModelAndView getTechnicianDashboard(){
		ModelAndView mav = new ModelAndView("techdashboard");
		int testUserId =4;
		WorkOrderDashboardView dashboardView =workOrderService.getDashboardDetailsForTechnician(testUserId);
		mav.addObject("dashboardView", dashboardView);
		return mav;
	}
	@RequestMapping(value="/assignedworkorders")
	public ModelAndView getAssignedWorkOrders(){
		ModelAndView mav = new ModelAndView("assignedworkorders");
		long testUser = 4;
		List<WorkOrder>workOrders=workOrderService.getAssignedWorkOrdersByUser(testUser);
			String statusMsg = "Found " + workOrders.size() +" assigned work orders";
		logger.info(statusMsg);
		mav.addObject("assignedWorkOrders", workOrders);
		return mav;
	}
	@RequestMapping(value="/openWorkOrders")
	public ModelAndView getOpenWorkOrders(){
		ModelAndView mav = new ModelAndView("openworkorders");
		long testUser = 4;
		List<WorkOrder>workOrders=workOrderService.getOpenWorkOrdersByUser(testUser);
			String statusMsg = "Found " + workOrders.size() +" open work orders";
		logger.info(statusMsg);
		mav.addObject("openWorkOrders", workOrders);
		return mav;
	}
	@RequestMapping(value="/testTile")
	public ModelAndView getTestTile(){
		ModelAndView mav = new ModelAndView("tile");
		return mav;
	}
	
	@RequestMapping(value="/editWorkOrder")
	public ModelAndView editWorkOrder(){
		ModelAndView mav = new ModelAndView("openworkorders");
		return mav;
	}
	@RequestMapping(value="/workOrderDetail")
	public ModelAndView getWorkOrderDetail(@RequestParam("workOrderId")long workOrderId){
		logger.info("Get work order details for workorder id: " + workOrderId);
		WorkOrder workOrder =workOrderService.getWorkOrderById(workOrderId);
		ModelAndView mav = new ModelAndView("workOrder");
		initializeWorkOrderBeans(mav);
		return mav;
	}


	@RequestMapping(value="/completedWorkOrders")
	public ModelAndView getCompletedWorkOrders(){
		ModelAndView mav = new ModelAndView("completedworkorders");
		long testUser = 4;
		List<WorkOrder>workOrders=workOrderService.getCompletedWorkOrdersByUser(testUser);
			String statusMsg = "Found " + workOrders.size() +" completed work orders";
		logger.info(statusMsg);
		mav.addObject("completedWorkOrders", workOrders);
		return mav;
	}
	@RequestMapping(value="/closedWorkOrders")
	public ModelAndView getClosedWorkOrders(){
		ModelAndView mav = new ModelAndView("closedworkorders");
		long testUser = 4;
		List<WorkOrder>closedWorkOrders =workOrderService.getClosedWorkOrdersByUser(testUser);
		mav.addObject("closedWorkOrders", closedWorkOrders);
		return mav;
	}
	@RequestMapping(value="/workorderview")
	public ModelAndView viewWorkOrder(){
		ModelAndView mav = new ModelAndView("workorder");
		return mav;
	}
	@RequestMapping(value="/createWorkOrder")
	public ModelAndView createWorkOrder(){
		WorkOrder workOrder = new WorkOrder();
		
		ModelAndView mav = new ModelAndView("workorder");
		WorkOrderView workOrderView = new WorkOrderView();
		workOrderView.setWorkOrder(workOrder);
		mav.addObject("workOrderView", workOrderView);
		initializeWorkOrderBeans(mav);
		return mav;
	}

	@RequestMapping(value="/submitWorkOrder", method = RequestMethod.POST)
	public ModelAndView submitWorkOrder(@ModelAttribute("workOrderView") WorkOrderView workOrderView, @ModelAttribute("assignedTo")AppUser technician){
		logger.info("Work order submitted with description: "+ workOrderView.getWorkOrder().getDescription());
		logger.info("Work order date reported: "+ workOrderView.getWorkOrder().getDateReported());
		logger.info("Work order date assigned: "+ workOrderView.getWorkOrder().getDateAssigned());
		logger.info("Work order assignedTo Id: "+ workOrderView.getSelectedTechnician());
		workOrderService.createWorkOrder(workOrderView.getWorkOrder(),Long.parseLong( workOrderView.getSelectedTechnician()), null);
		
		ModelAndView mav = new ModelAndView("workorder");
		initializeWorkOrderBeans(mav);
		mav.addObject("workOrderView", workOrderView);
		return mav;
	}

	private void initializeWorkOrderBeans(ModelAndView mav) {
		// load beans to be used by web page
		List<WorkOrderStatus>workOrderStatusList = workOrderService.getWorkOrderStatusList();
		List<Customer>customerList = customerService.getAllCustomers();
		List<AppUser>expeditors=appUserService.getAllExpeditors();
		List<AppUser>techs=appUserService.getAllTechs();
		mav.addObject("expeditors",expeditors);
		mav.addObject("techs", techs);
		mav.addObject("customerList", customerList);
		mav.addObject("workOrderStatusList", workOrderStatusList);
		mav.addObject("assignedTo", new AppUser());
	}
}
