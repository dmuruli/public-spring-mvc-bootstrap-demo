package com.dmuruli.hermes.web.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.dmuruli.hermes.entity.AppUser;
@Component
public class UserValidator implements Validator {

	@Override
	public boolean supports(Class clazz) {
		return AppUser.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		AppUser user = (AppUser)obj;
		String email = user.getEmail();
		boolean foundUser=false;
		if(email.equalsIgnoreCase("tech1@tech.com")
				|| email.equalsIgnoreCase("admin1@tech.com")){
			foundUser=true;
			
		}
		if(!foundUser){
			errors.rejectValue("email", "User Does Not Exist in System");
		}
	}

}
