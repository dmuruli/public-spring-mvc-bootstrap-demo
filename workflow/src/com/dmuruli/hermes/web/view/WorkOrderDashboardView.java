package com.dmuruli.hermes.web.view;

public class WorkOrderDashboardView {

	long userId;
	String userName;
	long createdWorkOrdersCount;
	long assignedWorkOrdersCount; 
	long openWorkOrdersCount; 
	long completeWorkOrdersCount; 
	long closedWorkOrdersCount;
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the createdWorkOrdersCount
	 */
	public long getCreatedWorkOrdersCount() {
		return createdWorkOrdersCount;
	}
	/**
	 * @param createdWorkOrdersCount the createdWorkOrdersCount to set
	 */
	public void setCreatedWorkOrdersCount(long createdWorkOrdersCount) {
		this.createdWorkOrdersCount = createdWorkOrdersCount;
	}
	/**
	 * @return the assignedWorkOrdersCount
	 */
	public long getAssignedWorkOrdersCount() {
		return assignedWorkOrdersCount;
	}
	/**
	 * @param assignedWorkOrdersCount the assignedWorkOrdersCount to set
	 */
	public void setAssignedWorkOrdersCount(long assignedWorkOrdersCount) {
		this.assignedWorkOrdersCount = assignedWorkOrdersCount;
	}
	/**
	 * @return the openWorkOrdersCount
	 */
	public long getOpenWorkOrdersCount() {
		return openWorkOrdersCount;
	}
	/**
	 * @param openWorkOrdersCount the openWorkOrdersCount to set
	 */
	public void setOpenWorkOrdersCount(long openWorkOrdersCount) {
		this.openWorkOrdersCount = openWorkOrdersCount;
	}
	/**
	 * @return the completeWorkOrdersCount
	 */
	public long getCompleteWorkOrdersCount() {
		return completeWorkOrdersCount;
	}
	/**
	 * @param completeWorkOrdersCount the completeWorkOrdersCount to set
	 */
	public void setCompleteWorkOrdersCount(long completeWorkOrdersCount) {
		this.completeWorkOrdersCount = completeWorkOrdersCount;
	}
	/**
	 * @return the closedWorkOrdersCount
	 */
	public long getClosedWorkOrdersCount() {
		return closedWorkOrdersCount;
	}
	/**
	 * @param closedWorkOrdersCount the closedWorkOrdersCount to set
	 */
	public void setClosedWorkOrdersCount(long closedWorkOrdersCount) {
		this.closedWorkOrdersCount = closedWorkOrdersCount;
	} 
	
	
}
