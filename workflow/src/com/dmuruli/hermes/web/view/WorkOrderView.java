package com.dmuruli.hermes.web.view;

import com.dmuruli.hermes.entity.WorkOrder;

public class WorkOrderView {
WorkOrder workOrder;
String selectedTechnician;
String selectedCustomer;
/**
 * @return the workOrder
 */
public WorkOrder getWorkOrder() {
	return workOrder;
}
/**
 * @param workOrder the workOrder to set
 */
public void setWorkOrder(WorkOrder workOrder) {
	this.workOrder = workOrder;
}
/**
 * @return the selectedTechnician
 */
public String getSelectedTechnician() {
	return selectedTechnician;
}
/**
 * @param selectedTechnician the selectedTechnician to set
 */
public void setSelectedTechnician(String selectedTechnician) {
	this.selectedTechnician = selectedTechnician;
}
/**
 * @return the selectedCustomer
 */
public String getSelectedCustomer() {
	return selectedCustomer;
}
/**
 * @param selectedCustomer the selectedCustomer to set
 */
public void setSelectedCustomer(String selectedCustomer) {
	this.selectedCustomer = selectedCustomer;
}


}
