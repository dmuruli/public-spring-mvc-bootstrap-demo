package com.dmuruli.hermes.dao;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.dmuruli.hermes.entity.Customer;
import com.dmuruli.hermes.entity.WorkOrder;
@Component
public class CustomerDao {
	@PersistenceContext
	 private EntityManager entityManager;
	
	  @SuppressWarnings("unchecked")
	public List<Customer>findAll(){
		    List<Customer> customerList = new ArrayList<Customer>();
		    customerList =entityManager.createNamedQuery("findAllCustomers").getResultList();
		    return customerList;
	  }
}
