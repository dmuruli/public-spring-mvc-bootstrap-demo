package com.dmuruli.hermes.dao;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.dmuruli.hermes.entity.WorkOrder;
@Component
public class WorkOrderDao {
	@PersistenceContext
	 private EntityManager entityManager;
	
	  @SuppressWarnings("unchecked")
	public List<WorkOrder>findAll(){
		    List<WorkOrder> workOrderList = new ArrayList<WorkOrder>();
		    workOrderList =entityManager.createNamedQuery("findAllWorkOrders").getResultList();
		    return workOrderList;
		  }
	  public int findAssignedWorkOrdersCount(long appUserId){
		  int assignedWorkOrdersCount =0;
		  assignedWorkOrdersCount = findWorkOrdersByStatusCount( appUserId, 2L);
		  return assignedWorkOrdersCount;
	  }
	  public int findWorkOrdersByStatusCount(long appUserId, long workOrderStatusId){
		  Long assignedWorkOrdersCount =0L;
		  assignedWorkOrdersCount =(Long) entityManager.createNamedQuery("findWorkOrderCountByStatus")
				  .setParameter("workOrderStatusId", workOrderStatusId)
				  .setParameter("appUserId", appUserId)
				  .getSingleResult();
		  return assignedWorkOrdersCount.intValue();
	  }
	  @SuppressWarnings("unchecked")
	public List<WorkOrder> findWorkOrdersByStatus(long appUserId, Long statusId){
		  List<WorkOrder> workOrderList = new ArrayList<WorkOrder>();
		  workOrderList =entityManager.createNamedQuery("findWorkOrdersByStatus")
				  .setParameter("workOrderStatusId", statusId)
				  .setParameter("appUserId", appUserId)
				  .getResultList();
		  
		  return workOrderList;
	  }
	  private List<WorkOrder> findWorkOrdersByNamedQuery(long appUserId, String namedQuery){
		  List<WorkOrder> workOrderList = new ArrayList<WorkOrder>();
		  workOrderList =entityManager.createNamedQuery(namedQuery).getResultList();	  
		  return workOrderList;
	  }
	  
	  public int findOpenWorkOrders(long appUserId){
		  int openWorkOrdersCount =0;
		  return openWorkOrdersCount;
	  }
	  public int findCompletedWorkOrders(long appUserId){
		  int completedWorkOrdersCount =0;
		  
		  return completedWorkOrdersCount;
	  }
	  public WorkOrder saveWorkOrder(WorkOrder workOrder){
		     WorkOrder savedWorkOrder =entityManager.merge(workOrder);      
		     return savedWorkOrder;
		  }
	  public void createWorkOrder(WorkOrder workOrder){
		  entityManager.persist(workOrder);  
	  }
	public WorkOrder findById(long workOrderId) {
		 WorkOrder workOrder = entityManager.find(WorkOrder.class, new Long(workOrderId));
		return workOrder;
	}
}
