package com.dmuruli.hermes.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.dmuruli.hermes.entity.AppUser;
import com.dmuruli.hermes.entity.Customer;
@Component
public class AppUserDao {
	@PersistenceContext
	 private EntityManager entityManager;
	
	public List<AppUser>getAllTechs(){
		List<AppUser> techs = new ArrayList<AppUser>();
		techs =entityManager.createNamedQuery("findAllTechs").getResultList();
	    return techs;
	}
	public List<AppUser>getAllExpeditors(){
		List<AppUser> expeditors = new ArrayList<AppUser>();
		expeditors =entityManager.createNamedQuery("findAllExpeditors").getResultList();
	    return expeditors;
	}
	public AppUser findById(Long appUserId) {
		AppUser user=entityManager.find(AppUser.class, appUserId);
		return user;
	}
}
