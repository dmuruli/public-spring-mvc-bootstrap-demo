package com.dmuruli.hermes.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.dmuruli.hermes.entity.WorkOrderStatus;
@NamedQuery(name="findAllWorkOrders", query="select w From WorkOrder w")
@Component
public class WorkOrderStatusDao extends BaseDao {
	@PersistenceContext
	 private EntityManager entityManager;
	public List<WorkOrderStatus>findAll(){
		List<WorkOrderStatus> workOrderStatusList = new ArrayList<WorkOrderStatus>();
		workOrderStatusList=entityManager.createNamedQuery("findAllWorkOrderStatus").getResultList();;
		return workOrderStatusList;
	}
}
