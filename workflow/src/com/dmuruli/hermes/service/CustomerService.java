package com.dmuruli.hermes.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dmuruli.hermes.dao.CustomerDao;
import com.dmuruli.hermes.entity.Customer;
@Service
public class CustomerService {
	@Autowired
	CustomerDao customerDao;
	public List<Customer>getAllCustomers(){
		List<Customer>customerList =  new ArrayList<Customer>();
		customerList=customerDao.findAll();
		return customerList;
	}
}
