package com.dmuruli.hermes.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dmuruli.hermes.dao.AppUserDao;
import com.dmuruli.hermes.entity.AppUser;

@Service
public class AppUserService {
	@Autowired
	AppUserDao appUserDao;
	public List<AppUser>getAllTechs(){
		List<AppUser>techs = new ArrayList<AppUser>();
		techs=appUserDao.getAllTechs();
		return techs;
	}
	public List<AppUser>getAllExpeditors(){
		List<AppUser>expeditors = new ArrayList<AppUser>();
		expeditors=appUserDao.getAllExpeditors();
		return expeditors;
	}

}
