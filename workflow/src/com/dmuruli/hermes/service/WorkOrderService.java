package com.dmuruli.hermes.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dmuruli.hermes.dao.AppUserDao;
import com.dmuruli.hermes.dao.WorkOrderDao;
import com.dmuruli.hermes.dao.WorkOrderStatusDao;
import com.dmuruli.hermes.entity.AppUser;
import com.dmuruli.hermes.entity.WorkOrder;
import com.dmuruli.hermes.entity.WorkOrderStatus;
import com.dmuruli.hermes.web.controller.WorkOrderController;
import com.dmuruli.hermes.web.view.WorkOrderDashboardView;

@Service
public class WorkOrderService {
	@Autowired
	WorkOrderDao workOrderDao;
	@Autowired
	WorkOrderStatusDao workOrderStatusDao;
	@Autowired
	AppUserDao appUserDao;
	private static final Logger logger = Logger.getLogger(WorkOrderService.class);
	public List<WorkOrder>getAllWorkOrdersByUser(long userid){
		List<WorkOrder> workOrders =new ArrayList<WorkOrder>();
		workOrders =workOrderDao.findAll();
		return workOrders;
		
	}
	public WorkOrderDashboardView getDashboardDetailsForTechnician(long appUserId){
		WorkOrderDashboardView dashboardView = new WorkOrderDashboardView();
		
		int createdViewCount =workOrderDao.findWorkOrdersByStatusCount(appUserId, 
				WorkOrderStatus.STATUS_CODE.CREATED.getValue());
		int assignedViewCount =workOrderDao.findWorkOrdersByStatusCount(appUserId, 
				WorkOrderStatus.STATUS_CODE.ASSIGNED.getValue());
		int openViewCount =workOrderDao.findWorkOrdersByStatusCount(appUserId, 
				WorkOrderStatus.STATUS_CODE.OPEN.getValue());
		int completeViewCount =workOrderDao.findWorkOrdersByStatusCount(appUserId, 
				WorkOrderStatus.STATUS_CODE.COMPLETE.getValue());
		int closedViewCount =workOrderDao.findWorkOrdersByStatusCount(appUserId, 
				WorkOrderStatus.STATUS_CODE.CLOSED.getValue());
		
		dashboardView.setCreatedWorkOrdersCount(createdViewCount);
		dashboardView.setAssignedWorkOrdersCount(assignedViewCount);
		dashboardView.setOpenWorkOrdersCount(openViewCount);
		dashboardView.setCompleteWorkOrdersCount(completeViewCount);
		dashboardView.setClosedWorkOrdersCount(closedViewCount);
		
		return dashboardView;
		
	}
	public List<WorkOrder> getCompletedWorkOrdersByUser(long userid){
		List<WorkOrder> workOrders =new ArrayList<WorkOrder>();
		Long statusCode = new Long(WorkOrderStatus.STATUS_CODE.COMPLETE.getValue());
		workOrders=workOrderDao.findWorkOrdersByStatus(userid,statusCode );
		return workOrders;
	}
	public List<WorkOrder> getAssignedWorkOrdersByUser(long userid){
		List<WorkOrder> workOrders =new ArrayList<WorkOrder>();
		Long statusCode = new Long(WorkOrderStatus.STATUS_CODE.ASSIGNED.getValue());
		workOrders=workOrderDao.findWorkOrdersByStatus(userid,statusCode );
		return workOrders;
	}
	public List<WorkOrder> getCreatedWorkOrdersByUser(long userid){
		List<WorkOrder> workOrders =new ArrayList<WorkOrder>();
		Long statusCode = new Long(WorkOrderStatus.STATUS_CODE.CREATED.getValue());
		workOrders=workOrderDao.findWorkOrdersByStatus(userid,statusCode );
		return workOrders;
	}
	public List<WorkOrder> getClosedWorkOrdersByUser(long userid){
		List<WorkOrder> workOrders =new ArrayList<WorkOrder>();
		Long statusCode = new Long(WorkOrderStatus.STATUS_CODE.CLOSED.getValue());
		workOrders=workOrderDao.findWorkOrdersByStatus(userid,statusCode );
		return workOrders;
	}
	public List<WorkOrder> getOpenWorkOrdersByUser(long userid){
		List<WorkOrder> workOrders =new ArrayList<WorkOrder>();
		Long statusCode = new Long(WorkOrderStatus.STATUS_CODE.OPEN.getValue());
		workOrders=workOrderDao.findWorkOrdersByStatus(userid,statusCode );
		return workOrders;
	}
	public List<WorkOrderStatus> getWorkOrderStatusList(){
		List<WorkOrderStatus>workOrderStatus = new ArrayList<WorkOrderStatus>();
		workOrderStatus = workOrderStatusDao.findAll();
		return workOrderStatus;
	}
	@Transactional
	public void saveWorkOrder(WorkOrder workOrder) {
		workOrderDao.createWorkOrder(workOrder);
		
	}
	@Transactional
	public void createWorkOrder(WorkOrder workOrder, long technicianId, AppUser admin) {
		// hard code now, but remove and make dynamic
		Long expeditorId =new Long(4);
		logger.info("Looking up tech with id:" + technicianId);
		AppUser tech = appUserDao.findById(technicianId);
		AppUser expeditor = appUserDao.findById(expeditorId);
		//because coming in might have just added the id and not the full object
		workOrder.setExpeditor(expeditor);
		String adminUserName ="";
		if(admin==null){
			adminUserName="SYSTEM_USER";
		}else 
		{
			adminUserName=admin.getFirstName() + " " + admin.getLastName();
		}
		workOrder.getTechnicians().add(tech);
		workOrder.setCreatedBy(adminUserName);
		workOrder.setCreatedTimeStamp(new Date());
		workOrderDao.createWorkOrder(workOrder);
	}
	public WorkOrder getWorkOrderById(long workOrderId) {
		WorkOrder workOrder = workOrderDao.findById(workOrderId);
		return workOrder;
	}
	
}
